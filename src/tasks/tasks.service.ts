import { Injectable, NotFoundException } from '@nestjs/common';
import { TaskRepository } from './task.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './task.entity';
import { CreateTaskDto } from './dto/create-task.dto';
import { TaskStatus } from './task-status.enum';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate/index';
import { User } from '../auth/user.entity';

@Injectable()
export class TasksService {
  constructor(@InjectRepository(TaskRepository)
              private readonly taskRepository: TaskRepository) {
  }

  async getTasks(filterDto: GetTasksFilterDto, user: User, options: IPaginationOptions): Promise<Pagination<Task>> {
    return await this.taskRepository.getTasks(filterDto, user, options);
  }


  async getTaskById(id: number, user: User): Promise<Task> {
    const found = await this.taskRepository.findOne({ where: { id, userId: user.id } });
    if (!found) throw new NotFoundException(`Task with ID ${id} not found`);

    return found;
  }

  async createTask(createTaskDto: CreateTaskDto, user: User): Promise<Task> {
    return await this.taskRepository.createTask(createTaskDto, user);
  }

  async deleteTask(id: number, user: User): Promise<void> {
    const result = await this.taskRepository.delete({ id, userId: user.id });
    if (!result.affected) throw new NotFoundException(`Task with ID ${id} not found`);
  }

  async updateTaskStatus(id: number, status: TaskStatus, user: User): Promise<Task> {
    const found: Task = await this.getTaskById(id, user);
    found.status = status;
    await found.save();
    return found;
  }
}
