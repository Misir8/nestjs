import { EntityRepository, Repository } from 'typeorm';
import { Task } from './task.entity';
import { CreateTaskDto } from './dto/create-task.dto';
import { TaskStatus } from './task-status.enum';
import { User } from '../auth/user.entity';
import { IPaginationOptions, paginate, Pagination } from 'nestjs-typeorm-paginate/index';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';

@EntityRepository(Task)
export class TaskRepository extends Repository<Task> {


  async getTasks(filterDto: GetTasksFilterDto, user: User, options: IPaginationOptions): Promise<Pagination<Task>> {
    const { status, search } = filterDto;
    const query = this.createQueryBuilder('task');
    query.where('task.userId = :userId', { userId: user.id });
    if (status) {
      query.andWhere('task.status = :status', { status });
    }

    if (search) {
      query.andWhere('task.title LIKE :search', { search: `%${search}%` });
    }

    const pagination = await paginate<Task>(query, options);
    pagination.meta.totalItems = await this.count();
    pagination.meta.totalPages = Math.ceil(pagination.meta.totalItems / pagination.meta.itemCount);
    return pagination;
  }

  async createTask(createTaskDto: CreateTaskDto, user: User) {
    const { title, description } = createTaskDto;
    const task = new Task();
    task.title = title;
    task.description = description;
    task.status = TaskStatus.OPEN;
    task.user = user;
    await task.save();
    delete task.user;
    return task;
  }
}
