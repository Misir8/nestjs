import { EntityRepository, Repository } from 'typeorm';
import { User } from './user.entity';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { HttpException, HttpStatus } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

@EntityRepository(User)
export class UserRepository extends Repository<User> {

  async signUp(authCredentialsDto: AuthCredentialsDto): Promise<void> {
    const { username, password } = authCredentialsDto;
    const exists = await this.findOne({ username });
    if (exists)
      throw new HttpException(`user with username ${username} exists`, HttpStatus.BAD_REQUEST);

    const user = new User();
    user.username = username;
    user.salt = await bcrypt.genSalt();
    user.password = await this.hashPassword(password, user.salt);
    console.log(user.password);
    await user.save();
  }

  async validateUserPassword(authCredentialsDto: AuthCredentialsDto): Promise<User> {
    const { username, password } = authCredentialsDto;
    const user = await this.findOne({ username });
    if(user && await user.validatePassword(password)){
      return user;
    }else{
      return null;
    }
  }

  private hashPassword = async (password: string, salt: string) => await bcrypt.hash(password, salt);
}
