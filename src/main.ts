import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as config from 'config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const server = process.env.PORT || config.get('server');
  await app.listen(server.port);
}
bootstrap();
